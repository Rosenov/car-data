pragma solidity 0.4.24;

contract CarData {
    //=================================STATE=====================================
    struct CarDataStruct {
        uint256 information_updated_at;
        uint256 last_oil_change;
        uint256 last_service_visit;
        uint256 kms;
        string year;
        string country;
        string color;
    }

    address owner;
    bool is_init_called = false;
    mapping (string => CarDataStruct) cars;

    //constructor
    constructor()   {
        owner = msg.sender;
    }

    //=================================MODIFIERS=====================================
    modifier isOwner {
        require(msg.sender == owner);
        _;
    }

    //=================================EVENTS=====================================
    event logCarDataInserted(string _vin_code, uint256 _information_updated_at, uint256 _last_oil_change, uint256 _last_service_visit, uint256 _kms, string _year, string _country, string _color);


    //=================================BODY=====================================
    function insertCarData(string _vin_code, uint256 _last_oil_change, uint256 _last_service_visit, uint256 _kms, string _year, string _country, string _color) public isOwner {
    //check for unique vin_code (mapping key)
        require(cars[_vin_code].information_updated_at == 0);
        cars[_vin_code] = CarDataStruct({information_updated_at: now, last_oil_change: _last_oil_change, last_service_visit: _last_service_visit, kms: _kms, year: _year, country: _country, color: _color});
        emit logCarDataInserted(_vin_code, cars[_vin_code].information_updated_at, cars[_vin_code].last_oil_change, cars[_vin_code].last_service_visit, cars[_vin_code].kms, cars[_vin_code].year, cars[_vin_code].country, cars[_vin_code].color);
    }

    function getCarData(string _vin_code) public view returns(uint256, uint256, uint256, uint256, string, string, string) {
        return (cars[_vin_code].information_updated_at, cars[_vin_code].last_oil_change, cars[_vin_code].last_service_visit, cars[_vin_code].kms, cars[_vin_code].year, cars[_vin_code].country, cars[_vin_code].color);
    }

    function init() isOwner {
        if(!is_init_called) {

        is_init_called = true;
        }
    }
}
