pragma solidity 0.4.24;

import "./Helper.sol";
import "./BaseContract.sol";

contract Ballot is Helper {
    address owner;

    constructor() public payable {
        owner = msg.sender;
    }

    //events
    event logChangedValue(uint256 _value);

    //modifiers
    modifier isOwner {
        require(msg.sender == owner);
        _;
    }

    //body
    function getOwner() view public returns(address)  {
        return owner;
    }

    function getContractBalance() view public returns(uint256)  {
        return address(this).balance;
    }

    function sumTwoNumbers(uint _a, uint _b) view returns (uint) {
        return add(_a, _b);
    }
}