var CarData = artifacts.require("./CarData.sol");

contract("CarData", function(accounts) {
    //setting up the account[1] will lead to revert, because account[1] is not allowed to call the method
    it('should revert the transaction of insertCarData when the from parameter is not the owner of the contract', function() {
        return CarData.deployed()
        .then(function (instance) {
            return instance.insertCarData("BG1234566", 1529767983, 1529767983, 1529767983, "1994", "Bulgaria", "Red", {from: accounts[0]});
        });
    });
});