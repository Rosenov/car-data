$(document).ready(function() {
    App.init();
});

App = {
    web3Provider: null,
    contracts: {},
    loading: false,
    init: function() {
        return App.initWeb3();
    },
    initWeb3: function() {
        // initialize web3
        if(typeof web3 !== 'undefined') {
            //reuse the provider of the Web3 object injected by Metamask
            App.web3Provider = web3.currentProvider;
            App.account = web3.eth.accounts[0];
        } else {
            //create a new provider and plug it directly into our local node
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:9545');
        }
        web3 = new Web3(App.web3Provider);
        return App.initContract();
    },

    initContract: function() {
        $.getJSON('../../build/contracts/CarData.json', function(chainListArtifact) {
            // get the contract artifact file and use it to instantiate a truffle contract abstraction
            App.contracts.CarData = TruffleContract(chainListArtifact);
            // set the provider for our contracts
            App.contracts.CarData.setProvider(App.web3Provider);
        });
    },
    insertCarData: function(vin_code, last_oil_change, last_service_visit, kms, year, country, color)  {
        App.contracts.CarData.deployed().then(function(instance) {
            return instance.insertCarData(vin_code, last_oil_change, last_service_visit, kms, year, country, color, {
                from: App.account,
                gas: 200000
            });
        }).then(function(result) {
            App.events.logCarDataInserted();
        }).catch(function(err) {
            console.error(err);
            bootbox.alert("Something went wrong.");
        });
    },
    getCarData: function(vin_code)  {
        App.contracts.CarData.deployed().then(function(instance) {
            return instance.getCarData.call(vin_code);
        }).then(function(result) {
            bootbox.alert("Last date information updated: "+formatDate(result[0].valueOf())+"<br>Last oil change: "+formatDate(result[1].valueOf())+"<br>Last service visit: "+formatDate(result[2].valueOf())+"<br>Kilometers passed: "+result[3].valueOf()+"<br>Year of manufacture: "+result[4]+"<br>Country: "+result[5]+"<br>Color: "+result[6]+"<br");
        }).catch(function(err) {
            console.error(err);
        });
    },
    events: {
        logCarDataInserted: function() {
            App.contracts.CarData.deployed().then(function(instance) {
                return instance.logCarDataInserted({}, {}).watch(function(error, event) {
                    if (!error) {
                        console.log(event);
                    } else {
                        console.error(error);
                    }
                });
            });
        }
    }
};

//init bootstrap date picker calendar style
$(".styled-date-picker").datepicker({
    dateFormat: 'yy/mm/dd',
    changeYear: true
});

$(".styled-date-picker-year").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
});

//On inserting car data form submission
$("form.insert-car-data").on("submit", function(event)   {
    event.preventDefault();
    var this_form = $(this);
    var errors = "";

    for(let i = 0, len = this_form.find("input.form-input").length; i < len; i+=1)  {
        if(this_form.find("input.form-input").eq(i).val().trim() == "") {
            errors+=this_form.find("input.form-input").eq(i).attr("data-required-msg")+"<br>";
        }

        if(this_form.find("input.form-input").eq(i).is("input[type='number']") && parseInt(this_form.find("input.form-input").eq(i).val().trim()) < 0) {
            errors+=this_form.find("input.form-input").eq(i).attr("data-positive-number-msg")+"<br>";
        }
    }

    if(!errors) {
        App.insertCarData(this_form.find("#vin_code").val().trim(), getTimestampFromDate(this_form.find("#last_oil_change").val().trim()),  getTimestampFromDate(this_form.find("#last_service_visit").val().trim()), this_form.find("#kms").val().trim(), this_form.find("#years_of_manufacture").val().trim(), this_form.find("#country").val().trim(), this_form.find("#color").val().trim());
    }else {
        bootbox.alert(errors);
    }
});

function getTimestampFromDate(el) {
    return new Date(el).getTime()/1000;
}

//On inserting car data form submission
$("form.get-car-data").on("submit", function(event)   {
    event.preventDefault();
    var this_form = $(this);
    var errors = "";

    if(this_form.find("#get_vin_code").val().trim() == "")  {
        errors = this_form.find("#get_vin_code").attr("data-required-msg");
    }

    if(!errors) {
        App.getCarData(this_form.find("#get_vin_code").val().trim());
    }else {
        bootbox.alert(errors);
    }
});

function formatDate(date) {
    var d = new Date(date * 1000),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('-');
}